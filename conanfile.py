from conans import ConanFile, CMake, tools
import os

class DragonboxConan(ConanFile):
    name = "dragonbox"
    version = "1.0.0"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Dragonbox here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"install_to_chars": [True, False], "fPIC": [True, False]}
    default_options = {"install_to_chars": True, "fPIC": True}
    generators = "cmake"

    def _cmake(self):
        cmake = CMake(self)
        cmake.definitions["DRAGONBOX_INSTALL_TO_CHARS"] = self.options.install_to_chars
        cmake.configure(source_folder="dragonbox")
        return cmake

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        os.rename("dragonbox-{}".format(self.version), "dragonbox")

    def build(self):
        cmake = self._cmake()
        cmake.build()

    def package(self):
        cmake = self._cmake()
        cmake.install()

    def package_info(self):
        if self.options.install_to_chars:
            self.cpp_info.libs = ["dragonbox_to_chars"]

